<?php

class Data_barang extends CI_Controller{
    public function index()
    {
        $data['barang']= $this->m_barang->tampil_data()->result();
        $this->load->view('templates_admin/header');
        $this->load->view('templates_admin/sidebar');
        $this->load->view('admin/data_barang',$data);
        $this->load->view('templates_admin/footer');
    }

    public function tambah_aksi(){
        $nama_barang = $this->input->post('nama_brg',true);
        $keterangan = $this->input->post('keterangan',true);
        $kategori = $this->input->post('kategori',true);
        $harga = $this->input->post('harga',true);
        $stock = $this->input->post('stock',true);
        $gambar	= $this->input->post('gambar', true);
        $gambar= $_FILES['gambar'];
          if ($gambar=''){}else{
              $config['upload_path'] = './uploads';
              $config['allowed_types'] ='jpg|png|gif';
  
              $this->load->library('upload',$config);
  
              if(!$this->upload->do_upload('gambar')){
                  echo "Upload Gagal"; die();
              }else{
                  $foto=$this->upload->data('file_name');
              }
          }


        $data = array(
            'nama_barang' =>$nama_barang,
            'keterangan' =>$keterangan,
            'kategori' =>$kategori,
            'harga' =>$harga,
            'stock' =>$stock,
            'gambar' =>$gambar
        );
        $this->m_barang->tambah_barang($data, 'tb_barang');
        redirect('data_barang/index');
    }
}

?>